# Flask Core
click==7.1.2
Flask==1.1.2
Jinja2==2.11.3
itsdangerous==1.1.0         # For checking untrusted sources https://pypi.org/project/itsdangerous/   https://www.kite.com/python/docs/itsdangerous           # For Email sender https://pypi.org/project/Flask-Mail/ https://www.youtube.com/watch?v=vF9n248M1yk 
MarkupSafe==1.1.1           # For Validate HTML or XML injection https://pypi.org/project/MarkupSafe/  https://www.kite.com/python/docs/markupsafe.Markup
Werkzeug==1.0.1

# Flask WTF Core
Flask-WTF==0.14.3
WTForms==2.2.1

# Extras
python-dotenv==0.12.0
requests==2.25.1
googletrans==3.1.0a0
dotmap==1.3.23

# Excel libraries 
xlrd==2.0.1
openpyxl==3.0.7
xlwt==1.3.0

# JWT libraries
PyJWT==2.3.0
