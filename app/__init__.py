'''
Modulo que inicia y configura la Aplicación de Flask
'''
from flask import Flask, render_template, session, redirect, url_for, request
from werkzeug.exceptions import HTTPException
from flask_wtf.csrf import CSRFProtect
from app.utils.constants import ReLoginRequired

# Dependencias propias
from app.utils.mixins import translate_error, set_session
from app.services.api_client import APIClient


def create_app():
    # Inicializar Aplicacion de Flask
    app = Flask(__name__, template_folder='templates', static_folder='static')

    with app.app_context():
        # Configuracion base de app
        app_config(app)

        # Setear sesion permanente
        app_session(app)

        # Definir e inicializar API CLient
        api_client(app)

        # Registrar rutas con vistas del app
        app_routes(app)

        # Manejador/Captura de errores
        app_error_handlers(app)

    return app


def app_config(app):
    # Definir variables de configuracion (desde config.py)
    app.config.from_object('config')

    # Aplicar seguridad de csrf para (formularios)
    csrf = CSRFProtect()
    csrf.init_app(app)


def app_session(app):
    @app.before_request
    def make_session_permanent():
        session.modified = True
        session.permanent = True


def api_client(app):
    api = APIClient(
        base_url=app.config['API_PATH'],
        headers={'Accept': '*/*'}
    )
    app.config['API_CLIENT'] = api


def app_routes(app):
    # Importar las vistas-blueprints
    from .views import Auth, Home, Ejes, Criterios, Certificaciones, TiposDocumento, Documentos, Usuarios
    from .views import Organizaciones, Proveedores, CatalogoDocumentos, CatalogoOrganizaciones, CatalogoCertificaciones

    # Registrar rutas para cada blueprint.
    app.register_blueprint(Auth, url_prefix='/')
    app.register_blueprint(Home, url_prefix='/')
    app.register_blueprint(Ejes, url_prefix='/admin')
    app.register_blueprint(Criterios, url_prefix='/admin')
    app.register_blueprint(Certificaciones, url_prefix='/admin')
    app.register_blueprint(Proveedores, url_prefix='/admin')
    app.register_blueprint(TiposDocumento, url_prefix='/admin')
    app.register_blueprint(Documentos, url_prefix='/admin')
    app.register_blueprint(Usuarios, url_prefix='/admin')
    app.register_blueprint(Organizaciones, url_prefix='/admin')
    app.register_blueprint(CatalogoDocumentos, url_prefix='/public')
    app.register_blueprint(CatalogoOrganizaciones, url_prefix='/public')
    app.register_blueprint(CatalogoCertificaciones, url_prefix='/public')


def app_error_handlers(app):
    error_template = 'error.html'
    error_logger = app.config['ERROR_LOGGER']

    # Capturar exception de ReLoginRequired
    @app.errorhandler(ReLoginRequired)
    def handle_relogin_required(error):
        set_session('redirect_url', request.referrer)
        return redirect(url_for('Auth.login'))

   # Capturar excepciones del sistema
    @app.errorhandler(Exception)
    def handle_exception(e):
        error_logger.error('EXCEPTION: '+str(e), exc_info=True)
        err = {
            'code': 500,
            'name': 'Internal Server Error',
            'description': 'An Error has occurred on the application server, you might contact the administrator for more information'
        }
        err = translate_error(err)
        return render_template(error_template, error=err, exception=True), 500

    # TODO Capturar el time-out
    # @app.errorhandler()
    # def handle_time_out():
    #

    # Capturar cualquier error http.
    @app.errorhandler(HTTPException)
    def handle_http_error(exc):
        err = translate_error(exc)
        return render_template(error_template, error=err), err['code']
