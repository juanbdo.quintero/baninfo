import json
from flask import Blueprint, render_template, redirect, url_for, flash, request, current_app
from app.utils.mixins import current_date, set_session, get_session, set_multi_session
from app.utils.constants import MESSAGES

APP_CONFIG = current_app.config
api_client = APP_CONFIG['API_CLIENT']

TiposDocumento = Blueprint('TiposDocumento', __name__)


@TiposDocumento.route('/tiposdoc', methods=['GET', 'POST'])
def list_tiposdoc():
    # Setear menú Administración
    set_session('SelMenu', 'Administración')

    # Cargar listado
    r_listado = api_client.get(
        endpoint='tipos-documentos'
    )
    if not r_listado:
        r_listado = {}

    return render_template(
        'admin/tiposdoc/listar.html',
        titulo='Gestionar Tipos de Documento',
        acciones='edit,delete',
        datos=r_listado,
        datos_json=json.dumps(r_listado)
    )


@TiposDocumento.route('/tiposdoc/accion', methods=['GET', 'POST'])
def action_tipodoc():
    action = request.args.get('dbAction')
    item = request.form.to_dict()
    codigo = str(item.pop('codigo'))

    if action == 'create':
        endpoint_ = 'tipos-documentos'
        api_client.post(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_CREATING_RECORD, 'success')
    elif action == 'update':
        endpoint_ = 'tipos-documentos/' + codigo
        api_client.put(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_UPDATING_RECORD, 'success')

    return redirect(url_for('TiposDocumento.list_tiposdoc'))


#Recibe el codigo del tipo de documento desde la lista
@TiposDocumento.route('/tiposdoc/borrar', methods=['POST'])
def delete_tipodoc():
    endpoint_ = 'tipos-documentos/' + request.args.get('codigo')
    response = api_client.delete(
        endpoint=endpoint_
    )
    if response:
        flash(MESSAGES.SUCCESS_DELETING_RECORD, 'success')
    else:
        flash(MESSAGES.FAIL_DELETING_RECORD, 'danger')

    return redirect(url_for('TiposDocumento.list_tiposdoc'))
