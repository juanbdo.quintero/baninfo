from flask import Blueprint, render_template, current_app, request, session, send_file, flash, redirect, url_for
from app.utils.mixins import current_date, get_session, set_multi_session, set_session, random_secret
from app.utils.constants import MESSAGES
from app.utils.logic import set_parametros_encuesta, set_parametros_responsable

APP_CONFIG = current_app.config
api_client = APP_CONFIG['API_CLIENT']

Home = Blueprint('Home', __name__)

@Home.route('/')
@Home.route('/inicio')
def index():
    set_multi_session({
        'SelMenu': 'Inicio',
        'SelEjeNombre': '',
        'SelTipoNombre': '',
        'SelCategoria': '',
        'SelCriterio': '',
        'SelEtiqueta': '',
        'ConfigRol': 'Usuario',
        'ThemeColor': 'blue'
    })

    return render_template('inicio.html')
