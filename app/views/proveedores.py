import json
from flask import Blueprint, render_template, redirect, url_for, flash, request, current_app
from app.utils.mixins import current_date, set_session, get_session, set_multi_session
from app.utils.constants import MESSAGES
from app.views import criterios, organizaciones

APP_CONFIG = current_app.config
api_client = APP_CONFIG['API_CLIENT']

Proveedores = Blueprint('Proveedores', __name__)


@Proveedores.route('/ejes/criterios/proveedores', methods=['GET', 'POST'])
def list_proveedores():
    # Cargar datos del criterio
    criterio = request.args.get('criterio')
    s_criterio = api_client.get(
        endpoint='criterios/' + criterio
    )
    # Setear variables
    set_multi_session({
        'SelMenu': 'Administración',
        'SelCriterioCodigo': s_criterio['codigo'],
        'SelCriterioNombre': s_criterio['nombre']
    })

    # Cargar listado de proveedores
    r_listado = api_client.get(
        endpoint='proveedores/criterio/' + criterio
    )
    if not r_listado:
        r_listado = {}

    # Cargar listado de organizaciones para asociar
    r_organizaciones = api_client.get(
        endpoint='organizaciones'
    )
    if not r_organizaciones:
        r_organizaciones = {}

    return render_template(
        'admin/proveedores/listar.html',
        titulo='Proveedores del Criterio',
        acciones='edit,delete',
        datos=r_listado,
        organizaciones=r_organizaciones,
        datos_json=json.dumps(r_listado)
    )


@Proveedores.route('/ejes/criterios/proveedores/accion', methods=['GET', 'POST'])
def action_proveedor():
    action = request.args.get('dbAction')
    item = request.form.to_dict()
    set_session('SelCriterioCodigo', item['criterio'])

    if action == 'create':
        endpoint_ = 'proveedores'
        api_client.post(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_CREATING_RECORD, 'success')

    return redirect(url_for('Proveedores.list_proveedores') + '?criterio=' + str(item['criterio']))


#Recibe el id del responsable desde la lista
@Proveedores.route('/ejes/criterios/proveedores/borrar', methods=['POST'])
def delete_proveedor():
    criterio = request.args.get('criterio')
    organizacion = request.args.get('org')

    endpoint_ = 'proveedores/criterio/' + criterio + '/org/' + organizacion
    response = api_client.delete(
        endpoint=endpoint_
    )
    if response:
        flash(MESSAGES.SUCCESS_DELETING_RECORD, 'success')
    else:
        flash(MESSAGES.FAIL_DELETING_RECORD, 'danger')

    return redirect(url_for('Proveedores.list_proveedores') + '?criterio=' + str(criterio))
