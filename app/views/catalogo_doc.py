import json
from flask import Blueprint, render_template, request, current_app
from app.utils.mixins import set_session, get_session, set_multi_session


APP_CONFIG = current_app.config
api_client = APP_CONFIG['API_CLIENT']

CatalogoDocumentos = Blueprint('CatalogoDocumentos', __name__)

@CatalogoDocumentos.route('/catalogo/documentos', methods=['GET'])
def explore():
    set_multi_session({
        'SelMenu': 'Documentos',
        'SelEjeCodigo': '',
        'SelEjeNombre': '',
        'SelTipoCodigo': '',
        'SelTipoNombre': '',
        'SelCategoria': '',
        'SelEtiqueta': ''
    })

    # Cargar ejes para filtro
    r_ejes = get_api_filter('ejes')
    set_session('Ejes', r_ejes)

    # Cargar tipos de documentos para filtro
    r_tiposdoc = get_api_filter('tipos-documentos')
    set_session('TiposDoc', r_tiposdoc)

    # Cargar etiquetas para filtro
    r_etiquetas = get_api_filter('documentos/etiquetas')
    set_session('Etiquetas', r_etiquetas)

    # Cargar listado de documentos
    r_listado = api_client.get(
        endpoint='documentos'
    )
    if not r_listado:
        r_listado = {}

    return render_template(
        'public/documentos/explorar.html',
        titulo='Explorar Documentos',
        filtro_tit='',
        filtro_cat='',
        filtro_tag='',
        datos=r_listado,
        ejes=r_ejes,
        tipos=r_tiposdoc,
        etiquetas=r_etiquetas,
        datos_json=json.dumps(r_listado)
    )


@CatalogoDocumentos.route('/catalogo/documentos/eje', methods=['GET'])
def explore_x_eje():
    eje = request.args.get('codigo')

    set_multi_session({
        'SelMenu': 'Documentos',
        'SelEjeCodigo': eje,
        'SelTipoCodigo': '',
        'SelTipoNombre': '',
        'SelCategoria': '',
        'SelEtiqueta': ''
    })

    # Cargar ejes para filtro
    r_ejes = get_session_filter('Ejes', 'ejes')
    s_eje = api_client.get(
        endpoint='ejes/' + eje
    )
    set_session('SelEjeNombre', s_eje['nombre'])

    # Cargar tipos de documentos para filtro
    r_tiposdoc = get_session_filter('TiposDoc', 'tipos-documentos')

    # Cargar tipos de documentos para filtro
    r_etiquetas = get_session_filter('Etiquetas', 'documentos/etiquetas')

    # Cargar listado de documentos x eje
    r_listado = api_client.get(
        endpoint='documentos/eje/' + eje
    )
    if not r_listado:
        r_listado = {}

    return render_template(
        'public/documentos/explorar.html',
        titulo='Explorar Documentos',
        filtro_tit='Filtro por eje: ' + s_eje['nombre'],
        filtro_cat='eje',
        filtro_tag=s_eje['nombre'],
        datos=r_listado,
        ejes=r_ejes,
        tipos=r_tiposdoc,
        etiquetas=r_etiquetas,
        datos_json=json.dumps(r_listado)
    )


@CatalogoDocumentos.route('/catalogo/documentos/tipo', methods=['GET'])
def explore_x_tipo():
    tipo = request.args.get('codigo')

    set_multi_session({
        'SelMenu': 'Documentos',
        'SelEjeCodigo': '',
        'SelEjeNombre': '',
        'SelTipoCodigo': tipo,
        'SelCategoria': '',
        'SelEtiqueta': ''
    })

    # Cargar tipos de documentos para filtro
    r_tiposdoc = get_session_filter('TiposDoc', 'tipos-documentos')
    s_tipo = api_client.get(
        endpoint='tipos-documentos/' + tipo
    )
    set_session('SelTipoNombre', s_tipo['nombre'])

    # Cargar ejes para filtro
    r_ejes = get_session_filter('Ejes', 'ejes')

    # Cargar tipos de documentos para filtro
    r_etiquetas = get_session_filter('Etiquetas', 'documentos/etiquetas')

    # Cargar listado de documentos x tipo
    r_listado = api_client.get(
        endpoint='documentos/tipo/' + tipo
    )
    if not r_listado:
        r_listado = {}

    return render_template(
        'public/documentos/explorar.html',
        titulo='Explorar Documentos',
        filtro_tit='Filtro por tipo: ' + s_tipo['nombre'],
        filtro_cat='tipo',
        filtro_tag=s_tipo['nombre'],
        datos=r_listado,
        ejes=r_ejes,
        tipos=r_tiposdoc,
        etiquetas=r_etiquetas,
        datos_json=json.dumps(r_listado)
    )


@CatalogoDocumentos.route('/catalogo/documentos/categoria', methods=['GET'])
def explore_x_categoria():
    tag = request.args.get('tag')

    set_multi_session({
        'SelMenu': 'Documentos',
        'SelEjeCodigo': '',
        'SelEjeNombre': '',
        'SelTipoCodigo': '',
        'SelTipoNombre': '',
        'SelCategoria': tag,
        'SelEtiqueta': ''
    })

    # Cargar ejes para filtro
    r_ejes = get_session_filter('Ejes', 'ejes')

    # Cargar tipos de documentos para filtro
    r_tiposdoc = get_session_filter('TiposDoc', 'tipos-documentos')

    # Cargar etiquetas para filtro
    r_etiquetas = get_session_filter('Etiquetas', 'documentos/etiquetas')

    # Cargar listado de documentos x categoria
    r_listado = api_client.get(
        endpoint='documentos/categoria/' + tag
    )
    if not r_listado:
        r_listado = {}

    return render_template(
        'public/documentos/explorar.html',
        titulo='Explorar Documentos',
        filtro_tit='Filtro por categoría: ' + tag,
        filtro_cat='categoria',
        filtro_tag=tag,
        datos=r_listado,
        ejes=r_ejes,
        tipos=r_tiposdoc,
        etiquetas=r_etiquetas,
        datos_json=json.dumps(r_listado)
    )


@CatalogoDocumentos.route('/catalogo/documentos/etiqueta', methods=['GET'])
def explore_x_etiqueta():
    tag = request.args.get('tag')

    set_multi_session({
        'SelMenu': 'Documentos',
        'SelEjeCodigo': '',
        'SelEjeNombre': '',
        'SelTipoCodigo': '',
        'SelTipoNombre': '',
        'SelCategoria': '',
        'SelEtiqueta': tag
    })

    # Cargar ejes para filtro
    r_ejes = get_session_filter('Ejes', 'ejes')

    # Cargar tipos de documentos para filtro
    r_tiposdoc = get_session_filter('TiposDoc', 'tipos-documentos')

    # Cargar etiquetas para filtro
    r_etiquetas = get_session_filter('Etiquetas', 'documentos/etiquetas')

    # Cargar listado de documentos x etiqueta
    r_listado = api_client.get(
        endpoint='documentos/etiqueta/' + tag
    )
    if not r_listado:
        r_listado = {}

    return render_template(
        'public/documentos/explorar.html',
        titulo='Explorar Documentos',
        filtro_tit='Filtro por etiqueta: ' + tag,
        filtro_cat='etiqueta',
        filtro_tag=tag,
        datos=r_listado,
        ejes=r_ejes,
        tipos=r_tiposdoc,
        etiquetas=r_etiquetas,
        datos_json=json.dumps(r_listado)
    )


# Cargar datos para los filtros de selección
def get_session_filter(session_var, end_point):
    data = get_session(session_var)
    if not data:
        data = get_api_filter(end_point)
    return data

def get_api_filter(end_point):
    data = api_client.get(
        endpoint=end_point
    )
    if not data:
        data = {}
    return data
