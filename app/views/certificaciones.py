import json
from flask import Blueprint, render_template, redirect, url_for, flash, request, current_app
from app.utils.mixins import current_date, set_session, get_session, set_multi_session
from app.utils.constants import MESSAGES
from app.views import criterios

APP_CONFIG = current_app.config
api_client = APP_CONFIG['API_CLIENT']

Certificaciones = Blueprint('Certificaciones', __name__)

# Esto se debe hacer cuando se cargan los parámetros del componente


@Certificaciones.route('/ejes/criterios/certificaciones', methods=['GET', 'POST'])
def list_certificaciones():
    # Cargar datos del criterio
    criterio = request.args.get('criterio')
    s_criterio = api_client.get(
        endpoint='criterios/' + criterio
    )
    # Setear variables
    set_multi_session({
        'SelMenu': 'Administración',
        'SelCriterioCodigo': s_criterio['codigo'],
        'SelCriterioNombre': s_criterio['nombre']
    })

    # Cargar listado de certificaciones
    r_listado = api_client.get(
        endpoint='certificaciones/criterio/' + str(criterio)
    )
    if not r_listado:
        r_listado = {}

    return render_template(
        'admin/certificaciones/listar.html',
        titulo='Certificaciones del Criterio',
        acciones='edit,delete',
        datos=r_listado,
        datos_json=json.dumps(r_listado)
    )


@Certificaciones.route('/ejes/criterios/certificaciones/accion', methods=['GET', 'POST'])
def action_certificacion():
    action = request.args.get('dbAction')
    item = request.form.to_dict()
    codigo = str(item.pop('codigo'))
    criterio = get_session('SelCriterioCodigo')

    if action == 'create':
        endpoint_ = 'certificaciones'
        api_client.post(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_CREATING_RECORD, 'success')
    elif action == 'update':
        endpoint_ = 'certificaciones/' + codigo
        api_client.put(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_UPDATING_RECORD, 'success')

    return redirect(url_for('Certificaciones.list_certificaciones') + '?criterio=' + str(criterio))


#Recibe el codigo del eje desde la lista
@Certificaciones.route('/ejes/criterios/certificaciones/borrar', methods=['POST'])
def delete_certificacion():
    codigo = request.args.get('codigo')
    criterio = get_session('SelCriterioCodigo')
    endpoint_ = 'certificaciones/' + str(codigo)
    response = api_client.delete(
        endpoint=endpoint_
    )
    if response:
        flash(MESSAGES.SUCCESS_DELETING_RECORD, 'success')
    else:
        flash(MESSAGES.FAIL_DELETING_RECORD, 'danger')

    return redirect(url_for('Certificaciones.list_certificaciones') + '?criterio=' + str(criterio))
