import json
from datetime import datetime
from flask import Blueprint, render_template, redirect, url_for, flash, request, current_app
from app.utils.constants import MESSAGES
from app.utils.mixins import set_session, get_session

APP_CONFIG = current_app.config
api_client = APP_CONFIG['API_CLIENT']

Documentos = Blueprint('Documentos', __name__)

@Documentos.route('/documentos', methods=['GET', 'POST'])
def list_documentos():
    # Setear menú Administración
    set_session('SelMenu', 'Administración')

    # Cargar listado
    r_listado = api_client.get(
        endpoint='documentos'
    )
    if not r_listado:
        r_listado = {}

    # Cargar tipos de documentos para la lista de selección
    r_tiposdoc = get_api_filter('tipos-documentos')

    # Cargar ejes para la lista de selección
    r_ejes = get_api_filter('ejes')

    return render_template(
        'admin/documentos/listar.html',
        titulo='Gestionar Documentos',
        acciones='edit,delete',
        datos=r_listado,
        tiposdoc=r_tiposdoc,
        ejes=r_ejes,
        datos_json=json.dumps(r_listado)
    )


#Recibe el codigo del documento desde la lista
@Documentos.route('/documentos/accion', methods=['POST'])
def action_documento():
    action = request.args.get('dbAction')
    item = request.form.to_dict()
    codigo = str(item.pop('codigo'))
    item['etiquetas'] = item['etiquetas'].split(',')

    if action == 'create':
        item['fecha'] = get_current_date()
        item['usuario'] = get_session('UserId')
        endpoint_ = 'documentos'
        api_client.post(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_CREATING_RECORD, 'success')
    elif action == 'update':
        endpoint_ = 'documentos/' + codigo
        api_client.put(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_UPDATING_RECORD, 'success')

    return redirect(url_for('Documentos.list_documentos'))


#Recibe el codigo del tipo de documento desde la lista
@Documentos.route('/documentos/borrar', methods=['GET', 'POST'])
def delete_documento():
    endpoint_ = 'documentos/' + request.args.get('codigo')
    response = api_client.delete(
        endpoint=endpoint_
    )
    if response:
        flash(MESSAGES.SUCCESS_DELETING_RECORD, 'success')
    else:
        flash(MESSAGES.FAIL_DELETING_RECORD, 'danger')

    return redirect(url_for('Documentos.list_documentos'))


# Cargar datos para los filtros de selección
def get_session_filter(session_var, end_point):
    data = get_session(session_var)
    if not data:
        data = get_api_filter(end_point)
    return data

def get_api_filter(end_point):
    data = api_client.get(
        endpoint=end_point
    )
    if not data:
        data = {}
    return data


def get_current_date():
    formato = "%Y-%m-%d"
    fecha = datetime.now()
    fecha_formateada = datetime.strftime(fecha, formato) 
    return fecha_formateada
