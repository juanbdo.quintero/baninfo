import json
from flask import Blueprint, render_template, redirect, url_for, flash, request, current_app
from app.utils.mixins import current_date, set_session
from app.utils.constants import MESSAGES

APP_CONFIG = current_app.config
api_client = APP_CONFIG['API_CLIENT']

Organizaciones = Blueprint('Organizaciones', __name__)


@Organizaciones.route('/organizaciones', methods=['GET', 'POST'])
def list_organizaciones():
    # Setear menú Administración
    set_session('SelMenu', 'Administración')

    # Cargar listado
    r_listado = api_client.get(
        endpoint='organizaciones'
    )
    if not r_listado:
        r_listado = {}

    #print(r_listado)

    return render_template(
        'admin/organizaciones/listar.html',
        titulo='Gestionar Organizaciones',
        acciones='edit,delete',
        datos=r_listado,
        datos_json=json.dumps(r_listado)
    )


@Organizaciones.route('/organizaciones/accion', methods=['GET', 'POST'])
def action_organizacion():
    action = request.args.get('dbAction')
    item = request.form.to_dict()

    if action == 'create':
        endpoint_ = 'organizaciones'
        api_client.post(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_CREATING_RECORD, 'success')
    elif action == 'update':
        nombre = str(item.pop('nombre'))
        endpoint_ = 'organizaciones/' + nombre
        api_client.put(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_UPDATING_RECORD, 'success')

    return redirect(url_for('Organizaciones.list_organizaciones'))


#Recibe el id del responsable desde la lista
@Organizaciones.route('/organizaciones/borrar', methods=['GET', 'POST'])
def delete_organizacion():
    endpoint_ = 'organizaciones/' + request.args.get('nombre')
    response = api_client.delete(
        endpoint=endpoint_
    )
    if response:
        flash(MESSAGES.SUCCESS_DELETING_RECORD, 'success')
    else:
        flash(MESSAGES.FAIL_DELETING_RECORD, 'danger')

    return redirect(url_for('Organizaciones.list_organizaciones'))
