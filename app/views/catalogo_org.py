import json
from flask import Blueprint, render_template, request, current_app
from app.utils.mixins import set_session, get_session, set_multi_session
from datetime import datetime

APP_CONFIG = current_app.config
api_client = APP_CONFIG['API_CLIENT']

CatalogoOrganizaciones = Blueprint('CatalogoOrganizaciones', __name__)

@CatalogoOrganizaciones.route('/catalogo/organizaciones', methods=['GET'])
def explore():
    set_multi_session({
        'SelMenu': 'Organizaciones',
        'SelEjeCodigo': '',
        'SelEjeNombre': '',
        'SelCriterioCodigo': '',
        'SelCriterioNombre': ''
    })

    # Cargar ejes para filtro
    r_ejes = get_api_filter('ejes')
    set_session('Ejes', r_ejes)

    # Cargar criterios para filtro
    r_criterios = get_api_filter('criterios')
    set_session('Criterios', r_criterios)

    # Cargar listado de documentos
    r_listado = api_client.get(
        endpoint='organizaciones'
    )
    if not r_listado:
        r_listado = {}

    return render_template(
        'public/organizaciones/explorar.html',
        titulo='Explorar Organizaciones',
        filtro_tit='',
        filtro_cat='',
        filtro_tag='',
        datos=r_listado,
        ejes=r_ejes,
        criterios=r_criterios,
        datos_json=json.dumps(r_listado)
    )


@CatalogoOrganizaciones.route('/catalogo/organizaciones/eje', methods=['GET'])
def explore_x_eje():
    eje = request.args.get('codigo')

    set_multi_session({
        'SelMenu': 'Organizaciones',
        'SelEjeCodigo': eje,
        'SelCriterioCodigo': '',
        'SelCriterioNombre': ''
    })

    # Cargar ejes para filtro
    r_ejes = get_session_filter('Ejes', 'ejes')
    s_eje = api_client.get(
        endpoint='ejes/' + eje
    )
    set_session('SelEjeNombre', s_eje['nombre'])

    # Cargar criterios para filtro
    r_criterios = get_session_filter('Criterios', 'criterios')

    # Cargar listado de documentos x eje
    r_listado = api_client.get(
        endpoint='organizaciones/eje/' + eje
    )
    if not r_listado:
        r_listado = {}

    return render_template(
        'public/organizaciones/explorar.html',
        titulo='Explorar Organizaciones',
        filtro_tit='Filtro por eje: ' + s_eje['nombre'],
        filtro_cat='eje',
        filtro_tag=s_eje['nombre'],
        datos=r_listado,
        ejes=r_ejes,
        criterios=r_criterios,
        datos_json=json.dumps(r_listado)
    )


@CatalogoOrganizaciones.route('/catalogo/organizaciones/criterio', methods=['GET'])
def explore_x_criterio():
    criterio = request.args.get('codigo')

    set_multi_session({
        'SelMenu': 'Organizaciones',
        'SelEjeCodigo': '',
        'SelEjeNombre': '',
        'SelCriterioCodigo': criterio
    })

    # Cargar criterios para filtro
    r_criterios = get_session_filter('Criterios', 'criterios')
    s_criterio = api_client.get(
        endpoint='criterios/' + criterio
    )
    set_session('SelCriterioNombre', s_criterio['nombre'])

    # Cargar ejes para filtro
    r_ejes = get_session_filter('Ejes', 'ejes')

    # Cargar listado de documentos x tipo
    r_listado = api_client.get(
        endpoint='organizaciones/criterio/' + criterio
    )
    if not r_listado:
        r_listado = {}

    return render_template(
        'public/organizaciones/explorar.html',
        titulo='Explorar Organizaciones',
        filtro_tit='Filtro por criterio: ' + s_criterio['nombre'],
        filtro_cat='criterio',
        filtro_tag=s_criterio['nombre'],
        datos=r_listado,
        ejes=r_ejes,
        criterios=r_criterios,
        datos_json=json.dumps(r_listado)
    )


# Cargar datos para los filtros de selección
def get_session_filter(session_var, end_point):
    data = get_session(session_var)
    if not data:
        data = get_api_filter(end_point)
    return data

def get_api_filter(end_point):
    data = api_client.get(
        endpoint=end_point
    )
    if not data:
        data = {}
    return data
