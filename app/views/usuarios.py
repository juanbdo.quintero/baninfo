import json
from flask import Blueprint, render_template, redirect, url_for, flash, request, current_app
from app.utils.mixins import current_date, get_session, set_session, sha256_hash
from app.utils.constants import MESSAGES

APP_CONFIG = current_app.config
api_client = APP_CONFIG['API_CLIENT']

Usuarios = Blueprint('Usuarios', __name__)


@Usuarios.route('/usuarios', methods=['GET', 'POST'])
def list_usuarios():
    # Setear menú Administración
    set_session('SelMenu', 'Administración')

    # Cargar listado
    r_listado = api_client.get(
        endpoint='usuarios'
    )
    if not r_listado:
        r_listado = {}

    #print(r_listado)

    return render_template(
        'admin/usuarios/listar.html',
        titulo='Gestionar Usuarios',
        acciones='edit,delete',
        datos=r_listado,
        datos_json=json.dumps(r_listado)
    )


@Usuarios.route('/usuarios/accion', methods=['GET', 'POST'])
def action_usuario():
    action = request.args.get('dbAction')
    item = request.form.to_dict()
    item['clave'] = sha256_hash(str(item['clave']))

    if action == 'create':
        endpoint_ = 'usuarios'
        api_client.post(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_CREATING_RECORD, 'success')
    elif action == 'update':
        identificacion = str(item.pop('identificacion'))
        endpoint_ = 'usuarios/' + identificacion
        api_client.put(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_UPDATING_RECORD, 'success')

    return redirect(url_for('Usuarios.list_usuarios'))


#Recibe el id del responsable desde la lista
@Usuarios.route('/usuarios/borrar', methods=['GET', 'POST'])
def delete_usuario():
    endpoint_ = 'usuarios/' + request.args.get('identificacion')
    response = api_client.delete(
        endpoint=endpoint_
    )
    if response:
        flash(MESSAGES.SUCCESS_DELETING_RECORD, 'success')
    else:
        flash(MESSAGES.FAIL_DELETING_RECORD, 'danger')

    return redirect(url_for('Usuarios.list_usuarios'))


#Recibe el id del responsable desde la lista
@Usuarios.route('/usuarios/clave', methods=['GET', 'POST'])
def key_usuario():
    # Setear menú Administración
    set_session('SelMenu', 'Cuenta')

    return render_template(
        'admin/usuarios/actualizar.html',
        titulo='Cambiar Clave',
        acciones='edit'
    )


#Recibe la clave nueva
@Usuarios.route('/usuarios/clave/actualizar', methods=['POST'])
def updatekey_usuario():
    item = dict(request.values)
    identificacion = get_session('UserId')

   # Cargar usuario
    r_user = api_client.get(
        endpoint='usuarios/' + identificacion
    )
    del r_user['identificacion']
    r_user['clave'] = sha256_hash(str(item['new_clave']))

    response = api_client.put(
        endpoint='usuarios/' + identificacion,
        body=r_user
    )
    if response:
        flash(MESSAGES.SUCCESS_CHANGING_KEY, 'success')
    else:
        flash(MESSAGES.FAIL_CHANGING_KEY, 'danger')

    return redirect(url_for('Home.index'))
