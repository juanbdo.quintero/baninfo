from functools import wraps
from app.utils.constants import MESSAGES
from app.utils.mixins import get_session, sha256_hash, set_session
from flask import session, Blueprint, render_template, redirect, flash, url_for, current_app, request
import jwt


Auth = Blueprint('Auth', __name__)
APP_CONFIG = current_app.config
api_client = APP_CONFIG['API_CLIENT']

################################### Decoradores para protejer rutas ################################
def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if api_client.is_authenticated:
            return render_template('utils/login.html'), 200
        return f(*args, **kwargs)
    return decorated_function


def logout_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not api_client.is_authenticated:
            return f(*args, **kwargs)
        return render_template('utils/inicio.html'), 200
    return decorated_function


##################################### Rutas de login y logout ####################################
@logout_required
@Auth.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        logout_logic()
        return redirect(url_for('Home.index', _anchor='login'))

    # Login user
    user_credentials = dict(request.values)
    if user_credentials:
        user = login_logic(user_credentials)

    # Otorgar acceso e ingresar
    if user:
        # Verificar si existe una url de redireccion
        redirect_url = get_session('redirect_url')
        if redirect_url:
            set_session('redirect_url', None)
            return redirect(redirect_url)

        return redirect(url_for('Documentos.list_documentos'))

    # Denegar acceso
    return redirect(url_for('Home.index'))


@login_required
@Auth.route('/logout', methods=['GET', 'POST'])
def logout():
    logout_logic()
    return redirect(url_for('Home.index'))


##################################### Logica de logout ##################################
def logout_logic():
    ''' Desloguear usuario Logica '''
    # Deauthenticated api client
    api_client.is_authenticated = False
    api_client.headers = {}

    # Clean session variables
    # Except flash_messages and redirect_url
    flashed_messages = get_session('_flashes', False)
    redirect_url = get_session('redirect_url')
    session.clear()

    # keep the flash messages and redirect_url
    set_session('_flashes', flashed_messages, False)
    set_session('redirect_url', redirect_url)


##################################### Logica de login ##################################
def login_logic(user_credentials):
    ''' Loguear usuario Logica '''
    correo = user_credentials['correo']
    user_credentials['clave'] = sha256_hash(user_credentials['clave'])

    # Check user credentials
    endpoint = 'auth/login'

    login_response = api_client.post(
        endpoint=endpoint,
        body=user_credentials
    )

    # Autorizar a la el api client de consumir endpoints protegidos
    if login_response:
        token = login_response['access_token']
        # Set Token
        api_client.add_header(
            {'Authorization': 'Bearer ' + token}
        )
        api_client.is_authenticated = True

        decoded_token = jwt.decode(
            token, APP_CONFIG['BANINFO_JWT_KEY'], ['HS256'])
        # Setear usuario de la session
        baninfo_user = decoded_token['sub']
        set_session('UserId', baninfo_user['identificacion'])
        set_session('UserNombre', baninfo_user['nombre'])
        set_session('UserEmail', correo)

        return decoded_token

    flash(MESSAGES.USER_INVALID.format(correo), 'danger')
    return {}
