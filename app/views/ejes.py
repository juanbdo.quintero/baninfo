import json
from flask import Blueprint, render_template, redirect, url_for, flash, request, current_app
from app.utils.mixins import current_date, set_session
from app.utils.constants import MESSAGES

APP_CONFIG = current_app.config
api_client = APP_CONFIG['API_CLIENT']

Ejes = Blueprint('Ejes', __name__)

# Esto se debe hacer cuando se cargan los parámetros del componente


@Ejes.route('/ejes', methods=['GET', 'POST'])
def list_ejes():
    # Setear menú Administración
    set_session('SelMenu', 'Administración')

    # Cargar listado de ejes del componente
    r_listado = api_client.get(
        endpoint='ejes'
    )
    if not r_listado:
        r_listado = {}

    return render_template(
        'admin/ejes/listar.html',
        titulo='Gestionar Ejes',
        acciones='edit,delete',
        datos=r_listado,
        datos_json=json.dumps(r_listado)
    )


@Ejes.route('/ejes/accion', methods=['GET', 'POST'])
def action_eje():
    action = request.args.get('dbAction')
    item = request.form.to_dict()
    codigo = str(item.pop('codigo'))

    if action == 'create':
        endpoint_ = 'ejes'
        api_client.post(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_CREATING_RECORD, 'success')
    elif action == 'update':
        endpoint_ = 'ejes/' + codigo
        api_client.put(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_UPDATING_RECORD, 'success')

    return redirect(url_for('Ejes.list_ejes'))


#Recibe el codigo del eje desde la lista
@Ejes.route('/ejes/borrar', methods=['POST'])
def delete_eje():
    endpoint_ = 'ejes/' + request.args.get('codigo')
    response = api_client.delete(
        endpoint=endpoint_
    )
    if response:
        flash(MESSAGES.SUCCESS_DELETING_RECORD, 'success')
    else:
        flash(MESSAGES.FAIL_DELETING_RECORD, 'danger')

    return redirect(url_for('Ejes.list_ejes'))
