import json
from flask import Blueprint, render_template, redirect, url_for, flash, request, current_app
from app.utils.mixins import current_date, set_session, get_session, set_multi_session
from app.utils.constants import MESSAGES

APP_CONFIG = current_app.config
api_client = APP_CONFIG['API_CLIENT']

Criterios = Blueprint('Criterios', __name__)

# Esto se debe hacer cuando se cargan los parámetros del componente


@Criterios.route('/ejes/criterios', methods=['GET', 'POST'])
def list_criterios():
    # Cargar datos del eje
    eje = request.args.get('eje')

    s_eje = api_client.get(
        endpoint='ejes/' + eje
    )
    # Setear variables
    set_multi_session({
        'SelMenu': 'Administración',
        'SelEjeCodigo': s_eje['codigo'],
        'SelEjeNombre': s_eje['nombre']
    })

    # Cargar listado de criterios
    r_listado = api_client.get(
        endpoint='criterios/eje/' + str(eje)
    )
    if not r_listado:
        r_listado = {}

    return render_template(
        'admin/criterios/listar.html',
        titulo='Criterios asociados al Eje',
        acciones='edit,delete',
        datos=r_listado,
        datos_json=json.dumps(r_listado)
    )


@Criterios.route('/ejes/criterios/accion', methods=['GET', 'POST'])
def action_criterio():
    action = request.args.get('dbAction')
    item = request.form.to_dict()
    codigo = str(item.pop('codigo'))
    eje = get_session('SelEjeCodigo')

    if action == 'create':
        endpoint_ = 'criterios'
        api_client.post(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_CREATING_RECORD, 'success')
    elif action == 'update':
        endpoint_ = 'criterios/' + codigo
        api_client.put(
            endpoint=endpoint_,
            body=item
        )
        flash(MESSAGES.SUCCESS_UPDATING_RECORD, 'success')

    return redirect(url_for('Criterios.list_criterios') + '?eje=' + str(eje))


#Recibe el codigo del eje desde la lista
@Criterios.route('/ejes/criterios/borrar', methods=['POST'])
def delete_criterio():
    codigo = request.args.get('codigo')
    eje = get_session('SelEjeCodigo')
    endpoint_ = 'criterios/' + str(codigo)
    response = api_client.delete(
        endpoint=endpoint_
    )
    if response:
        flash(MESSAGES.SUCCESS_DELETING_RECORD, 'success')
    else:
        flash(MESSAGES.FAIL_DELETING_RECORD, 'danger')

    return redirect(url_for('Criterios.list_criterios') + '?eje=' + str(eje))
