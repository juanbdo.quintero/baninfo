from functools import wraps
from flask import flash, current_app
from requests import get, post, put, delete
from app.utils.constants import MESSAGES, ReLoginRequired

FAIL_CREATION, FAIL_RESPONSE, FAIL_REQUEST_PARAMS, FAIL_AUTH = MESSAGES.API_CLIENT.values()

request_properties = dict(
    endpoint='',
    headers={},
    params=None,
    body=None,
    auth=None
)

http_methods = {
    'get': get,
    'post': post,
    'put': put,
    'delete': delete,
}


def check_request_params(f):
    @wraps(f)
    def decorated_function(self, *args, **kwargs):
        allowed_params = list(request_properties.keys())
        sended_params = list(kwargs.keys())

        req_props = {**kwargs}
        for key, item in request_properties.items():
            if key not in req_props:
                req_props[key] = item

        contains = [p in allowed_params for p in sended_params]
        if any(contains):
            return f(self, *args, **req_props)

        raise ValueError(FAIL_REQUEST_PARAMS)

    return decorated_function


class APIClient():

    __instance = None
    is_authenticated = False

    @staticmethod
    def get_instance():
        return APIClient.__instance

    def __init__(self, base_url, headers={}):
        if APIClient.__instance is not None:
            raise ReferenceError(
                FAIL_CREATION)
        self.base_url = base_url
        self.headers = headers
        APIClient.__instance = self

    def add_header(self, headers):
        self.headers = {**self.headers, **headers}

    def set_request_properties_object(self, **kwargs):
        endpoint, headers, params, body, auth = request_properties.keys()

        url_var = self.base_url + kwargs[endpoint]
        headers_var = {**self.headers, **kwargs[headers]}
        params_var = kwargs[params]
        json_var = kwargs[body]
        auth_var = kwargs[auth]

        return dict(
            url=url_var,
            headers=headers_var,
            params=params_var,
            json=json_var,
            auth=auth_var
        )

    def check_response(self, response, endpoint):
        method = response.request.method
        content_type = response.headers.get('content-type')
        status = str(response.status_code)
        body = None

        # Validar formato json
        if content_type == 'application/json':
            body = response.json()

        # Validar respuesta 2xx
        if status[0] == '2':
            return self.extract_data(body)

        # Validar jwt error
        self.jwt_interceptor(body)

        # flash(FAIL_RESPONSE.format(status, method, endpoint), 'danger')
        current_app.config['ERROR_LOGGER'].error(
            'API CLIENT HTTP ERROR: ' +
            FAIL_RESPONSE.format(status, method, endpoint)
        )
        return None

    @staticmethod
    def jwt_interceptor(body):
        if body is not None:
            # Validate JWT error msg
            if 'jwt-error' in body.keys():
                flash(FAIL_AUTH, 'warning')
                raise ReLoginRequired(FAIL_AUTH)

    @staticmethod
    def extract_data(body):
        # Extraer campo data
        if 'data' in body.keys():
            # Validar si el campo data no se encuantra vacio o nulo
            if body['data']:
                return body['data']
            return []
        return body

    @check_request_params
    def request(self, http_method, **kwargs):
        request_properties_object = self.set_request_properties_object(
            **kwargs
        )
        request_function = http_methods[http_method]
        response = request_function(
            **request_properties_object
        )

        return self.check_response(response, kwargs.get('endpoint'))

    def get(self, **kwargs):
        return self.request('get', **kwargs)

    def post(self, **kwargs):
        return self.request('post', **kwargs)

    def put(self, **kwargs):
        return self.request('put', **kwargs)

    def delete(self, **kwargs):
        return self.request('delete', **kwargs)

    def patch(self, **kwargs):
        return self.request('patch', **kwargs)
