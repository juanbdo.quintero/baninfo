

import string

from app.utils.mixins import get_session, set_session


def set_parametros_responsable(parametros):
    for item in parametros:
        param = item
        if '_' in param:
            param = item.replace('_', ' ')
        param = string.capwords(param).replace(' ', '')
        value = parametros[str(item)]
        set_session(param, str(value))


def set_parametros_encuesta(parametros):
    for item in parametros:
        param = item['nombre']
        if ' ' in param:
            param = string.capwords(param)
        param = param.replace(' ', '')
        value = item['valor']
        set_session(param, value)


def set_parametros_dependencia(parametros, k):
    for variable in parametros.keys():
        if variable[len(variable)-1:] == str(k):
            param = variable[:len(variable)-1]
            value = parametros[variable]
            set_session(param, value)

    if get_session('NivelDependencias') != get_session('NivelDependencia'):
        set_session('NivelActividades', get_session('SegundoNivelActividades'))


def set_actividades(actividades_dep, actividades_res, recursos_res):
    lista_actividades = []
    for act_dep in actividades_dep:
        valor = 0
        eficiencia = 0
        motivo_eficiencia = ''
        checked = ''
        accion = 'post'
        for act in actividades_res:
            if act_dep['codigo'] == act['codigo']:
                valor = act['valor']
                eficiencia = act['eficiencia']
                motivo_eficiencia = act['motivo_eficiencia']
                checked = 'checked'
                accion = 'put'

        recursos = []
        for rec in recursos_res:
            if rec['actividad'] == act_dep['codigo']:
                recursos.append(rec['recurso'])

        actividad = {'codigo': act_dep['codigo'],
                     'nombre': act_dep['nombre'],
                     'valor_bd': valor,
                     'valor': valor,
                     'eficiencia_bd': eficiencia,
                     'eficiencia': eficiencia,
                     'motivo_eficiencia_bd': motivo_eficiencia,
                     'motivo_eficiencia': motivo_eficiencia,
                     'recursos': recursos,
                     'recursos_bd': recursos,
                     'checked': checked,
                     'accion': accion
                     }

        lista_actividades.append(actividad)
    set_session('ListaActividades', lista_actividades)


# recursos_com: recursos del componente
def set_recursos(recursos_com):
    lista_recursos = []
    for rec in recursos_com:
        recurso = {'codigo': rec['codigo'],
                   'nombre': rec['nombre']
                   }
        lista_recursos.append(recurso)
    set_session('ListaRecursos', lista_recursos)


# Aquí se borran los recursos por actividad del responsable
def save_actividad(accion, actividad, valor, eficiencia, recursos_bd, recursos, api_client):
    ar_headers = {
        'periodo': get_session('Periodo'),
        'componente': get_session('Componente'),
        'nivel_dependencia': get_session('NivelDependencias'),
        'dependencia': get_session('Dependencia'),
        'nivel_actividad': get_session('NivelActividades'),
        'actividad': str(actividad),
        'responsable': get_session('Cedula'),
        'valor': str(valor),
        'eficiencia': str(eficiencia)
    }

    if accion == 'put':
        api_client.put(
            endpoint='encuesta/colectores-actividades-responsable',
            headers=ar_headers
        )
        save_recursos(actividad, recursos_bd, recursos, api_client)
    elif accion == 'post':
        api_client.post(
            endpoint='encuesta/colectores-actividades-responsable',
            headers=ar_headers
        )
        save_recursos(actividad, recursos_bd, recursos, api_client)
    elif accion == 'delete':
        delete_recursos(actividad, api_client)
        ar_headers.pop('valor')
        api_client.delete(
            endpoint='encuesta/colectores-actividades-responsable',
            headers=ar_headers
        )

def save_recursos(actividad, recursos_bd, recursos, api_client):

    for rec in recursos:
        #Insertar un recurso que se chequeó en una actividad
        if rec not in recursos_bd:
            api_client.post(
                endpoint='encuesta/recursos-actividad',
                headers={
                    'periodo': get_session('Periodo'),
                    'componente': get_session('Componente'),
                    'nivel_dependencia': get_session('NivelDependencias'),
                    'dependencia': get_session('Dependencia'),
                    'responsable': get_session('Cedula'),
                    'nivel_actividad': get_session('NivelActividades'),
                    'actividad': str(actividad),
                    'recurso': str(rec)
                }
            )

    for rec in recursos_bd:
        #Borrar un recurso que se deschequeó en una actividad
        if rec not in recursos:
            api_client.delete(
                endpoint='encuesta/recursos-actividad',
                headers={
                    'periodo': get_session('Periodo'),
                    'componente': get_session('Componente'),
                    'nivel_dependencia': get_session('NivelDependencias'),
                    'dependencia': get_session('Dependencia'),
                    'responsable': get_session('Cedula'),
                    'nivel_actividad': get_session('NivelActividades'),
                    'actividad': str(actividad),
                    'recurso': str(rec)
                }
            )


# Aquí se borran los recursos por actividad del responsable
def delete_recursos(actividad, api_client):
    api_client.delete(
        endpoint='encuesta/recursos-actividad-responsable',
        headers={
            'periodo': get_session('Periodo'),
            'componente': get_session('Componente'),
            'nivel_dependencia': get_session('NivelDependencias'),
            'dependencia': get_session('Dependencia'),
            'responsable': get_session('Cedula'),
            'nivel_actividad': get_session('NivelActividades'),
            'actividad': str(actividad)
        }
    )
