from dotmap import DotMap

messages = dict(
    USER_INVALID='Usuario y/o contraseña incorrectos, para "<b>{0}</b>" por favor intente de nuevo.',
    USER_NOT_EXIST='Usuario no existe.',
    USER_NOT_DEPENDENCY='Usuario no tiene una dependencia asignada.',
    USER_NOT_RESPONSE='Usuario no debe responder la encuesta este periodo.',
    FAIL_FILE_DOWNLOAD='Error en descarga, el archivo no existe.',
    FAIL_FILE_EXTENSION='Extensión de archivo incorrecta: {0}, Sólo se permiten archivos excel .xls & xlsx',
    FAIL_FILE_SAVING='Ocurrió un error guardando el archivo de datos',
    SUCCESS_FILE_SAVING='Archivo guardado con éxito!',
    SUCCESS_SAVING_REPORT='Su reporte de actividades ha sido guardado con éxito. Muchas gracias!',
    FAIL_SAVING_REPORT='Ocurrió un error guardando su reporte de actividades. Por favor, intente nuevamente.',
    SUCCESS_DOWNLOAD_FILE='Archivo descargado con éxito.',
    SUCCESS_CREATING_RECORD='Registro creado con éxito.',
    SUCCESS_UPDATING_RECORD='Registro actualizado con éxito.',
    SUCCESS_DELETING_RECORD='Registro borrado con éxito.',
    SUCCESS_CHANGING_KEY='Clave actualizada con éxito.',
    FAIL_CREATING_RECORD='Ocurrió un error creando el registro. Por favor, intente nuevamente.',
    FAIL_UPDATING_RECORD='Ocurrió un error actualizando el registro. Por favor, intente nuevamente.',
    FAIL_DELETING_RECORD='Ocurrió un error borrando el registro. Por favor, intente nuevamente.',
    FAIL_CHANGING_KEY='Ocurrió un error actualizando la Clave. Por favor, intente nuevamente.',
    TENANT_INVALID_LOGIN_METHOD='Este tipo de autenticación no esta permitida para su organización.',
    USER_NOT_COFIGURED_OIDC='El usuario {0} no esta configurado para responder el reporte de actividades, por favor verifique con el admin de costos.',
    FAIL_OIDC_AUTH='Ha fallado el proceso de autenticación con su organización. Intentelo de nuevo o contante al administrador del sistema',
    API_CLIENT=dict(
        FAIL_CREATION='Invalid creation of API Client is a singleton.',
        FAIL_RESPONSE='ERROR {0} en servicio {1} {2}.',
        FAIL_REQUEST_PARAMS='Invalid request params on API Client function.',
        FAIL_AUTH='Su sessión expiro, por favor loguearse de nuevo.'
    )
)
MESSAGES = DotMap(messages)

class ReLoginRequired(Exception):
    ''' Esta clase es una excepcion especifica que define el error de re logueo'''
