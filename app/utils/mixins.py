# Add your own utility classes and functions here.
# Rember separete the logic of the views and use_cases
import secrets
from random import randint
from flask import session
from wtforms.validators import DataRequired
from hashlib import sha256
from datetime import datetime
from googletrans import Translator
translator = Translator()


def random_secret():
    random_int = randint(10, 12)
    secret = secrets.token_hex(nbytes=random_int)
    return secret


def sha256_hash(texto):
    texto_encoded = texto.encode()
    texto_hasheado = sha256(texto_encoded).hexdigest()
    return str(texto_hasheado)


def save_params_in_session(parametros, update_parametro):
    for item in parametros:
        param = 'baninfo_' + item
        if session[param] != parametros[item]:
            session[param] = parametros[item]
            update_parametro(item, parametros[item])


def change_params_on_session(parametros, parametros_check, update_parametro):
    for variable in parametros.keys():

        param = parametros[variable]
        variable_session = 'baninfo_' + variable

        if session[variable_session] != param:
            session[variable_session] = param
            update_parametro(variable, param)

    for variable in parametros_check:
        checkvar = 'off'
        if variable in parametros.keys():
            checkvar = 'on'
        if session['baninfo_'+variable] != checkvar:
            session['baninfo_'+variable] = checkvar
            update_parametro(variable, checkvar)

# Translation functions


def translate_text(word):
    translation = translator.translate(str(word), src='en', dest='es')
    word_translation = translation.text
    return word_translation


def translate_error(err):
    try:
        if isinstance(err, dict):
            err['name'] = translate_text(err['name'])
            err['description'] = translate_text(err['description'])
        else:
            err = dict(
                code=err.code,
                name=translate_text(err.name),
                description=translate_text(err)
            )
    except Exception:
        return err
    return err


def current_date():
    now = datetime.now()
    formatted_date = now.strftime('%A, %d de %B.')
    return translate_text(formatted_date)


def get_session(key, compose_key=True):
    compose_key = 'baninfo_' + str(key) if compose_key else key
    return session.get(compose_key, None)


def set_session(key, value, compose_key=True):
    compose_key = 'baninfo_' + str(key) if compose_key else key
    session[compose_key] = value


def set_multi_session(variables):
    for key, value in variables.items():
        set_session(key, value)


def is_admin():
    return get_session('ConfigRol') == 'Administrador'
