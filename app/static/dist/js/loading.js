$ = jQuery;
var loadingAllowed =  true;
// Loading methods
function hideLoading() {
	$('#loading').hide();
}

function showLoading() {
	$('#loading').show();
}

// Ocultar loading cuando la pagina haya cargado los componentes 
$(window).on('load', function () {
	hideLoading();
});

// Mostrar loading cuando se navegue a travez de la pagina
window.addEventListener('beforeunload', () => {
	if(loadingAllowed) {
		showLoading();
	}else{
		loadingAllowed = true;
	}
});
// Excluir el loading cuando se de click en un elemento con la clase ignore-loading 
// Tambien se puede setear la varibale globar loadingAllowed = false; para que no aparezca en el proximo onload 
$(document).on('click', '.ignore-loading', function (e) {
	loadingAllowed = false;
})


const modals = ['#login', '#logout'];

if(modals.includes(window.location.hash)) {
  const modalId = window.location.hash + 'Modal' ;
  $(modalId).modal('toggle');
}
