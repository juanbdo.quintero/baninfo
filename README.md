# Banco de Información Web

Este proyecto es una aplicacion web cliente; desarrollada en Python Flask framework, que tiene como objetivo principal brindar un cliente web para el Proyecto Banco de Información de la Alcaldia de Medellín.

<br>


Para configurar y correr el proyecto...

1. Instalar Python 3.8+ y pip
2. Crear y activar un "virtualenv"
3. Instalar dependencias con el "requirements-dev.txt"
4. Correr el proyecto con "flask run" en la raiz

En los siguientes puntos se especifica mas sobre el proyecto base y el desarrollo de los puntos anteriores

<br>

### Estructura de Archivos:

    ├───.vscode
    ├───app
    │   ├───forms
    │   ├───static
    │   │   └───uploads
    │   ├───templates
    │   │   ├───admin
    │   │   ├───auth
    │   │   └───components
    │   ├───utils
    │   ├───views
    │   └───
    ├───logs
    ├───tests
    │   └───test_app_case.py
    │   └───test_app_client.py
    ├───venv
    ├───.env
    ├───.flaskenv
    ├───.gitignore
    ├───requirements.txt
    ├───README.md
    ├───config.py
    ├───run.py / wsgi.py

### Definición de Ambiente Virtual:

    $ pip install virtualenv
    $ virtualenv venv
    $ source venv/Scripts/activate

### Instalación:

    (venv) $ pip install -r requirements-dev.txt
    (venv) $ pip freeze

### Definición de Variables de Ambiente:

    (venv) $ touch .env
        API_PATH = "https://someapi.com"
        SECRET_KEY = "some-secret-key"

### Correr el proyecto en ambiente local:

    (venv) $ flask run --reload --with-threads


### Correr el proyecto en ambiente remoto (ubuntu):

    (venv) $ flask run --host=0.0.0.0 --port=5000

### Preparar el Editor:
<br>

Git Extensions:

https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens

https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory

https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph

<br>

Python Extensions:

https://marketplace.visualstudio.com/items?itemName=ms-python.python

https://marketplace.visualstudio.com/items?itemName=LittleFoxTeam.vscode-python-test-adapter

https://marketplace.visualstudio.com/items?itemName=njpwerner.autodocstring

https://marketplace.visualstudio.com/items?itemName=mgesbert.python-path

<br>

Flask Extensions:

https://marketplace.visualstudio.com/items?itemName=cstrap.flask-snippets

https://marketplace.visualstudio.com/items?itemName=wholroyd.jinja

https://marketplace.visualstudio.com/items?itemName=samuelcolvin.jinjahtml

<br>

Linters Extensions:

https://marketplace.visualstudio.com/items?itemName=usernamehw.errorlens

https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-spanish

<br>

HTML Templates Extensions:

https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag

https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag

https://marketplace.visualstudio.com/items?itemName=vincaslt.highlight-matching-tag

<br>

Extras:

https://marketplace.visualstudio.com/items?itemName=vscode-icons-team.vscode-icons

https://marketplace.visualstudio.com/items?itemName=TabNine.tabnine-vscode

https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree

https://marketplace.visualstudio.com/items?itemName=mikestead.dotenv

https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer

<br>

### DocString Plantilla

Para la extension de VSCode Python Docstring Generator

Function

    '''
    [Summary].

    :param   [name]<type>:  [description].

    :returns [name]<type>:  [description].
    '''

Class

    '''
    [Summary].
    '''

### Eso es todo!!
